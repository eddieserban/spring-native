#!/usr/bin/env bash
NS=titins
APP_NAME=customers
IMAGE_NAME=demo.io/customers/puki:latest

mvn -DskipTest=true clean package spring-boot:build-image -Dspring-boot.build-image.imageName=${IMAGE_NAME}

