package com.example.demo;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

record Customer(@Id Integer id, String name){}

@Service
interface CustomerService extends ReactiveCrudRepository<Customer , Integer>{

}

@RestController
@ResponseBody
class CustomerController {

	private final CustomerService customerService;

	CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@GetMapping("/customers")
	public Flux<Customer> getAll(){
		return customerService.findAll();
	}
}

@Component
class MyRunner implements ApplicationRunner {

	private final CustomerService customerService;

	MyRunner(CustomerService customerService) {
		this.customerService = customerService;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

		var just = Flux.just("Elena", "Puki", "Eddie");

		Flux<Customer> customerFlux = just.map(name -> new Customer(null, name)).flatMap(c -> customerService.save(c));


		customerService.deleteAll().thenMany(customerFlux).
				subscribe(System.out::println);


	}
}


@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
